package com.example.practica3c2;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import Modelo.AlumnoDbHelper;
import Modelo.AlumnosDb;

public class AlumnoAlta extends AppCompatActivity {
    private static final int REQUEST_IMAGE_SELECT = 1;
    private static final int REQUEST_CODE_SELECT_IMAGE = 100;
    private Uri selectedImageUri;
    private Button btnGuardar, btnRegresar, btnEliminar, btnImgAlumno;
    private Alumno alumno;
    private EditText txtNombre, txtMatricula, txtGrado;
    private ImageView imgAlumno;
    private TextView lblImagen;
    private String carrera = "Ing. Tec. Información";
    private int posicion;
    private String imagePath;


    private AlumnosDb alumnosDb;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_alumno_alta);
        btnGuardar = (Button) findViewById(R.id.btnGuardar);
        btnRegresar = (Button) findViewById(R.id.btnRegresar);
        btnEliminar = (Button) findViewById(R.id.btnEliminar);
        txtMatricula = (EditText) findViewById(R.id.txtMatricula);
        txtNombre = (EditText) findViewById(R.id.txtNombre);
        txtGrado = (EditText) findViewById(R.id.txtGrado);
        imgAlumno = (ImageView) findViewById(R.id.imgAlumno);
        btnImgAlumno = (Button) findViewById(R.id.btnSeleccionarImagen);
        btnEliminar.setEnabled(false);
        // Inicializa la base de datos
        alumnosDb = new AlumnosDb(this, new AlumnoDbHelper(this));

        Bundle bundle = getIntent().getExtras();
        alumno = (Alumno) bundle.getSerializable("alumno");
        posicion = bundle.getInt("posicion", posicion);

        if (posicion >= 0) {
            txtMatricula.setText(alumno.getMatricula());
            txtNombre.setText(alumno.getNombre());
            txtGrado.setText(alumno.getGrado());
            Uri imageUri = Uri.parse(alumno.getImg());
            imgAlumno.setImageURI(imageUri);
            btnEliminar.setEnabled(true);
        }

        btnGuardar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (alumno == null) {
                    alumno = new Alumno();
                    alumno.setGrado(txtGrado.getText().toString());
                    alumno.setMatricula(txtMatricula.getText().toString());
                    alumno.setNombre(txtNombre.getText().toString());

                    if (validar()) {
                        AlumnosDb alumnosDb = new AlumnosDb(getApplicationContext());

                        if (selectedImageUri != null) {
                            String imagePath = selectedImageUri.toString();
                            alumno.setImg(imagePath);
                        }
                        Aplicacion aplicacion = null;
                        Toast.makeText(getApplicationContext(), "Se creó con éxito", Toast.LENGTH_SHORT).show();


                        Aplicacion.alumnos.add(alumno);
                        alumnosDb.insertAlumno(alumno);
                        aplicacion.onCreate();
                        setResult(Activity.RESULT_OK);
                        finish();
                    } else {
                        Toast.makeText(getApplicationContext(), "Faltó capturar datos", Toast.LENGTH_SHORT).show();
                        txtMatricula.requestFocus();
                    }
                }
                if (posicion >= 0) {
                    alumno.setMatricula(txtMatricula.getText().toString());
                    alumno.setNombre(txtNombre.getText().toString());
                    alumno.setGrado(txtGrado.getText().toString());
                    if (selectedImageUri != null) {
                        String imagePath = selectedImageUri.toString();
                        alumno.setImg(imagePath);
                    }

                    Aplicacion.alumnos.get(posicion).setMatricula((alumno.getMatricula()));
                    Aplicacion.alumnos.get(posicion).setNombre(alumno.getNombre());
                    Aplicacion.alumnos.get(posicion).setGrado(alumno.getGrado());
                    Aplicacion.alumnos.get(posicion).setImg(alumno.getImg());
                    AlumnosDb alumnosDb = new AlumnosDb(getApplicationContext());
                    alumnosDb.updateALumno(alumno);

                    Toast.makeText(getApplicationContext(), "Se modificó con éxito", Toast.LENGTH_SHORT).show();
                    finish();
                }
            }
        });

        btnImgAlumno.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                seleccionarImagen();
            }
        });



        btnRegresar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setResult(Activity.RESULT_CANCELED);
                finish();
            }
        });

        btnEliminar.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Aplicacion aplicacion = null;
                AlumnosDb alumnosDba = new AlumnosDb(getApplicationContext());
                alumno.setId(Aplicacion.alumnos.get(posicion).getId());
                int id = alumno.getId();
                AlertDialog.Builder builder = new AlertDialog.Builder(AlumnoAlta.this);
                builder.setTitle("Confirmar eliminación");
                builder.setMessage("¿Deseas eliminar este alumno?");
                builder.setPositiveButton("Sí", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Aplicacion.alumnos.remove(posicion);
                        alumnosDba.deleteAlumnos(alumno.getId());
                        Toast.makeText(getApplicationContext(), "Alumno eliminado", Toast.LENGTH_SHORT).show();

                        aplicacion.onCreate();
                        setResult(Activity.RESULT_OK);
                        finish();
                    }
                });
                builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        // No se realiza ninguna acción
                    }
                });
                builder.show();
            }
        });
    }

    private void seleccionarImagen() {
        Intent intent = new Intent(Intent.ACTION_OPEN_DOCUMENT);
        intent.setType("image/*");
        startActivityForResult(intent, REQUEST_CODE_SELECT_IMAGE);
    }

    private boolean validar() {
        boolean exito = true;
        Log.d("nombre", "validar: " + txtNombre.getText());
        if (txtNombre.getText().toString().equals("")) exito = false;
        if (txtMatricula.getText().toString().equals("")) exito = false;
        if (txtGrado.getText().toString().equals("")) exito = false;

        return exito;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_CODE_SELECT_IMAGE && resultCode == RESULT_OK) {
            if (data != null && data.getData() != null) {
                selectedImageUri = data.getData();
                imgAlumno.setImageURI(selectedImageUri);
            }
        }
    }

    @Override
    protected void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        if (selectedImageUri != null) {
            outState.putString("selectedImageUri", selectedImageUri.toString());
        }
    }

    @Override
    protected void onRestoreInstanceState(@NonNull Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        if (savedInstanceState.containsKey("selectedImageUri")) {
            String uriString = savedInstanceState.getString("selectedImageUri");
            if (uriString != null) {
                selectedImageUri = Uri.parse(uriString);
                imgAlumno.setImageURI(selectedImageUri);
            }
        }
    }
}