package Modelo;

import com.example.practica3c2.Alumno;

public interface Persistencia {
    public void openDataBase();
    public void closeDataBase();
    public long insertAlumno (Alumno alumno);
    public long updateALumno (Alumno alumno);
    public void deleteAlumnos (int id);

}
